﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class giftController : Interactivo
{
    public enum TipoRegalo{
        Pokemon, Objeto
    }
    public TipoRegalo tipoRegalo;
    public Pokemon pokemon;
    public Botiquin objeto;

    private Animator pokeAnimator;

    // Start is called before the first frame update
    void Start()
    {
        pokeAnimator = this.GetComponent<Animator>();
        pokeAnimator.Play(this.gameObject.name);
    }
    private void LateUpdate()
    {
        if (!si.activo)
        {
            this.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 0);
            this.GetComponent<CapsuleCollider2D>().isTrigger = true;
            foreach (Transform child in this.transform)
            {
                child.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            }
        }
        else
        {
            if(!currentAnimationName().Equals(this.gameObject.name))
                pokeAnimator.Play(this.gameObject.name);
            this.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
            this.GetComponent<CapsuleCollider2D>().isTrigger = false;
            foreach (Transform child in this.transform)
            {
                child.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 90);
            }
        }
    }
    public override void Interactuar()
    {
        base.Interactuar();
        if (si.activo)
        {
            switch (tipoRegalo)
            {
                case TipoRegalo.Pokemon:
                    cD.playSE("getPokemon");
                    player.entrenador.listaPokemon.Add(pokemon); break;
                case TipoRegalo.Objeto:
                    cD.playSE("getItem");
                    player.entrenador.addInventario(objeto); break;
            }
            si.activo = false;
            Color c = this.GetComponent<SpriteRenderer>().color;
            c.a = 0;
            this.GetComponent<SpriteRenderer>().color = c;
            this.GetComponent<CapsuleCollider2D>().isTrigger = true;
            foreach(Transform child in this.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }

    public string currentAnimationName()//metodo para saber que animacion se esta reproduciendo
    {
        var currAnimName = "";
        foreach (AnimationClip clip in pokeAnimator.runtimeAnimatorController.animationClips)
        {
            if (pokeAnimator.GetCurrentAnimatorStateInfo(0).IsName(clip.name))
                currAnimName = clip.name.ToString();
        }
        return currAnimName;
    }
}
