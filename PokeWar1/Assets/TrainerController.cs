﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainerController : Interactivo
{
    [UnityEngine.Range(1, 2)]
    public int scene;

    public Entrenador e;
    
    override public void Interactuar()
    {
        if (si.activo)
        {
            base.Interactuar();/*
            player.eventing = true;
            StartCoroutine(procesarEventos());*/
            StartCoroutine(battle());
            si.activo = false;
        }
    }

    private IEnumerator battle()
    {
        while (player.eventing)
        {
            yield return null;
        }
        yield return new WaitForSeconds(1);
        player.startBattle(e, scene);
    }

}
