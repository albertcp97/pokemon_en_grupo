﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class PJ
{
    protected int movs=5;

    public string nombre;
    protected int hpmax;
    protected int hpact;
    protected int atk;
    protected int def;
    protected int mov;
    protected int vel;
    protected int expdada;
    protected int lvl;
    protected bool deb = false;
    protected bool move = false;
    protected bool atke = false;

    public PJ(string nombre, int hpmax, int mov, int atk, int def, int vel, int expdada, int lvl)
    {
        this.nombre = nombre;
        this.hpmax = hpmax;
        this.hpact = hpmax;
        this.mov = mov;
        this.atk = atk;
        this.def = def;
        this.vel = vel;
        this.expdada = expdada;
        this.lvl=lvl;
        deb = false;
    }

    public string getNombre()
    {
        return nombre;
    }
}

