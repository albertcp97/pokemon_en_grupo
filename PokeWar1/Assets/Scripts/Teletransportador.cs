﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Teletransportador : MonoBehaviour
{

    private Image imageDeCarga;
    [Range(0.01f, 0.1f)]
    public float velocidadAparecer = 0.025f;
    [Range(0.01f, 0.1f)]
    public float velocidadOcultar = 0.025f;

    private bool estaTeletransportandose;
    private PlayerController personaje;

    private void Start()
    {
        imageDeCarga = GameObject.Find("BlackImage").GetComponent<Image>();
        personaje = GameObject.Find("Player").gameObject.GetComponent<PlayerController>();
        imageDeCarga.gameObject.SetActive(false);
    }

    public void Transportar(Vector2 posicion, Direction direccion, bool moveForward)
    {
        if (!estaTeletransportandose)
        {
            personaje.cambiarLayer(-5);
            estaTeletransportandose = true;
            personaje.interacting = true;
            StartCoroutine(MostrarPantallaDeCarga(posicion, direccion, moveForward));
        }
    }


    private IEnumerator MostrarPantallaDeCarga(Vector2 posicion, Direction direccion, bool moveForward) //encontrado en internet y retocado, guia de pokemon en Unity2D: https://www.youtube.com/watch?v=e8TfXh_PieE
    {
        imageDeCarga.gameObject.SetActive(true);
        Color c = imageDeCarga.color;
        c.a = 0.0f;

        while (c.a < 1)
        {
            imageDeCarga.color = c;
            c.a += velocidadAparecer;
            yield return null;
        }

        personaje.interacting = false;
        personaje.cambiarPosicion(posicion, direccion);
        personaje.cambiarLayer(5);
        if (moveForward)
            personaje.activeForwardMovement();

        while (c.a > 0)
        {
            imageDeCarga.color = c;
            c.a -= velocidadOcultar;
            yield return null;
        }

        personaje.interacting = false;
        personaje.eventing = false;
        estaTeletransportandose = false;
        imageDeCarga.gameObject.SetActive(false);
    }

    public void TransportarEscena(Vector2 posicion, Direction direccion, int indexEscena)//igual que el otro teletransporte pero cambia de escena
    {
        if (!estaTeletransportandose)
        {
            estaTeletransportandose = true;
            personaje.interacting = true;
            StartCoroutine(MostrarPantallaDeCargaEscena(posicion, direccion, indexEscena));
        }
    }

    private IEnumerator MostrarPantallaDeCargaEscena(Vector2 posicion, Direction direccion, int indexEscena) //encontrado en internet y retocado, guia de pokemon en Unity2D: https://www.youtube.com/watch?v=e8TfXh_PieE
    {
        imageDeCarga.gameObject.SetActive(true);
        Color c = imageDeCarga.color;
        c.a = 0.0f;

        while (c.a < 1)
        {
            imageDeCarga.color = c;
            c.a += velocidadAparecer;
            yield return null;
        }

        personaje.cambiarPosicion(posicion, direccion);
        personaje.cambiarLayer(5);
        SceneManager.LoadScene(indexEscena);

        while (c.a > 0)
        {
            imageDeCarga.color = c;
            c.a -= velocidadOcultar;
            yield return null;
        }

        personaje.interacting = false;
        personaje.eventing = false;
        estaTeletransportandose = false;
        imageDeCarga.gameObject.SetActive(false);
    }

    public IEnumerator aparecerPantallaNegra()
    {
        imageDeCarga.gameObject.SetActive(true);
        Color c = imageDeCarga.color;
        c.a = 0.0f;

        while (c.a < 1)
        {
            imageDeCarga.color = c;
            c.a += velocidadAparecer;
            yield return null;
        }
    }
    public IEnumerator desvanecerPantallaNegra()
    {
        imageDeCarga.gameObject.SetActive(true);
        Color c = imageDeCarga.color;
        c.a = 1.0f;

        while (c.a > 0)
        {
            imageDeCarga.color = c;
            c.a -= velocidadOcultar;
            yield return null;
        }
        imageDeCarga.gameObject.SetActive(false);
    }

}
