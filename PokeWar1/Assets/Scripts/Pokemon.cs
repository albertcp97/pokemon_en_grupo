﻿
using System.Collections.Generic;
[System.Serializable]
public class Pokemon
{
    public string nombre;
    public int pokedex;
    public int mov;
    public int hpmax;
    public int atk;
    public int def;
    public int expdada;
    
    //private int ident;
    public List<Movimiento> movimientos = new List<Movimiento>();

    private void recibirDano(Pokemon p, Movimiento m) {
        //this.hpact=p.atk+a.
    }

    public Pokemon(string nombre, int pokedex, int mov, int hpmax, int atk, int def,int expdada, List<Movimiento> movimientos)
    {
        this.nombre = nombre;
        this.pokedex = pokedex;
        this.mov = mov;
        this.hpmax = hpmax;
        this.atk = atk;
        this.def = def;
        this.expdada = expdada;
        //this.ident=ident;
        this.movimientos = movimientos;

    }
    override public string ToString(){
        string s;
        s=this.nombre+" Nivel: "+20+" HP: "+this.hpmax + "/"+this.hpmax+" Ataque: "+this.atk+" Defensa: "+this.def+" Movimiento: "+this.mov;
        return s;
    }
    public void subirNivel(){
        if(this.atk<100){
            this.atk+=1;
        }
        if (this.def<90){
              this.def+=1;
        }
        this.hpmax+=1; 
    }
}
