﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Botiquin : Objeto
{
   private int cura;
   public Botiquin(string nombre, string nombres, string desc, int cura, int cant) : base(nombre,nombres,desc,cant){
       this.cura=cura;
   }
   override public string ToString(){
       if (this.cant!=1){
           return this.cant+" "+this.nombres+" '"+this.desc+"'";
       }
       else if(this.cant==1){
           return this.cant+" "+this.nombre+" '"+this.desc+"'";
       }
       return "";
   }

    public int getCura()
    {
        return cura;
    }
}
