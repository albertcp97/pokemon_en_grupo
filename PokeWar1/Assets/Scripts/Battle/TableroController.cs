﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableroController : MonoBehaviour
{
    public GameObject[] Players;
    // Start is called before the first frame update
    public Direction direccion;

    private Rigidbody2D playerBody;
    private Animator playerAnimator;


    private static GameObject player = null;

    private void Awake()
    {
        Application.targetFrameRate = 30;//fps del juego (move_Forward)
        //Player
        if (player == null)
        {
            player = this.gameObject;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
            Destroy(this);
        }
    }
    void Start()
    {
        playerBody = player.GetComponent<Rigidbody2D>();
        playerBody.gravityScale = 0;
        playerBody.constraints = RigidbodyConstraints2D.FreezeRotation;
        playerAnimator = player.GetComponent<Animator>();
        GameObject play = Instantiate(Players[0]);
        play.transform.SetParent(this.transform);
        play.transform.position=new Vector2(5, 5);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
