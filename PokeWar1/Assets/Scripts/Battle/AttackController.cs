﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class AttackController : MonoBehaviour
{
    public Movimiento ataque;
    public bool canMove;
    public PJController pjAtacante = null;
    public PJController pjAliado   = null;

    private PJController pjEnemigo = null;
    private char dir;
    private bool moving;
    private float initialPositionX;
    private float initialPositionY;
    private ConfirmColl confirmColl;

    // Start is called before the first frame update
    private void Start()
    {
        canMove = true;
        foreach (Transform child in this.transform) //navego entre los objetos hijos
        {
            if (child.gameObject.GetComponent<ConfirmColl>() != null)
                this.confirmColl = child.gameObject.GetComponent<ConfirmColl>();
        }
        if (confirmColl == null)
        {
            Debug.Log("Error te falta poner el objeto CheckColl en un personaje");
        }
    }
    private void FixedUpdate()
    {
        this.GetComponent<SpriteRenderer>().sortingOrder = (int)(103 - this.transform.position.y);
        foreach (Transform child in transform)
        {
            child.GetComponent<SpriteRenderer>().sortingOrder = (int)(103 - this.transform.position.y);
        }
    }
    // Update is called once per frame
    private void Update()
    {
        if (canMove)
        {
            moveCasilla();
            if (Input.GetKeyDown(KeyCode.Space) && pjEnemigo!=null)//podra pulsar si debajo tiene a un personaje no aliado
            {
                foreach (Transform child in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
                {
                    Destroy(child.gameObject);
                }
                canMove = false;

                decimal dividendo = decimal.Divide(pjAtacante.pokemon.atk, 100);
                double mult = ataque.poder * (double)dividendo;
                pjEnemigo.RecibirDano(ataque.poder + mult);

                StartCoroutine(this.desactivarAtaque());

                pjAtacante.movido = true;
                GameObject.Find("ActionManager").GetComponent<ActionManager>().FuckGoBack();
            }
            else if (Input.GetKeyDown(KeyCode.Space) && pjAliado != null && pjAliado.isPokemon)//podra pulsar si debajo tiene a un personaje no aliado
            {
                foreach (Transform child in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
                {
                    Destroy(child.gameObject);
                }
                canMove = false;

                pjAliado.CurarVida(pjAtacante.entrenador.takeFirstBotiquin().getCura());

                StartCoroutine(this.desactivarAtaque());

                pjAtacante.movido = true;
                GameObject.Find("ActionManager").GetComponent<ActionManager>().FuckGoBack();
            }
        }
    }
    private void moveCasilla()
    {
        if (!moving)
        {
            if (Input.GetKey("w")) dir = 'w';
            if (Input.GetKey("a")) dir = 'a';
            if (Input.GetKey("s")) dir = 's';
            if (Input.GetKey("d")) dir = 'd';
            if (Input.anyKey) moving = true;
            if (moving)
            {
                initialPositionX = this.transform.position.x;
                initialPositionY = this.transform.position.y;
            }
        }
        else
        {
            predictCollision(dir);
            BattleSeleter bs = GameObject.Find("Selecter").GetComponent<BattleSeleter>();
            if (moving) //si no detecta una collision
            {
                Vector3 targetPosition = new Vector3(initialPositionX, initialPositionY); //inicializo objetivo
                if (dir == 'w')
                    targetPosition = new Vector3(initialPositionX, initialPositionY + bs.distSqaure);
                else if (dir == 'a')
                    targetPosition = new Vector3(initialPositionX - bs.distSqaure, initialPositionY);
                else if (dir == 's')
                    targetPosition = new Vector3(initialPositionX, initialPositionY - bs.distSqaure);
                else if (dir == 'd')
                    targetPosition = new Vector3(initialPositionX + bs.distSqaure, initialPositionY);
                //Direccion en la que se mueve (1,0)=derecha (-1,0)=izquierda (0,1)=arriba (0,-1)=abajo //0 1 aparecen por el normalized
                Vector3 moveDir = (targetPosition - transform.position).normalized;//tambien podria ser por ejemplo derecha y abajo --> (1,-1)
                //Para adaptarse a la velocidad del ordenador utilizamos Time.deltaTime
                float moveAmount = bs.speedSquare * Time.deltaTime;
                //A partir de la velocidad calculada la reajustamos con Mathf.Clamp(numero, min, max),
                //si el movimiento sobrepasa el objetivo se reajustara al maximo dado por parametros en este caso la diferencia de distancia al objetivo
                //cuando sea necesario utilizar el maximo se hará la última suma a la posicion
                moveAmount = Mathf.Clamp(moveAmount, 0f, Vector3.Distance(transform.position, targetPosition));
                //sumar cantidad de movimiento
                transform.position += moveDir * moveAmount;
                if (Vector3.Distance(transform.position, targetPosition) == 0.0f)//diferencia entre el battleSelecter y el objetivo
                {
                    moving = false;
                    dir = ' ';
                    confirmColl.transform.position = this.transform.position;// new Vector3(this.transform.position.x, initialPositionY + distSqaure);
                }
            }
        }
    }
    private void predictCollision(char dir)
    {
        BattleSeleter bs = GameObject.Find("Selecter").GetComponent<BattleSeleter>();
        //Primero movemos el selecter pequeño a la siguiente casilla
        if (dir == 'w')
            confirmColl.transform.position = new Vector3(initialPositionX, initialPositionY + bs.distSqaure);
        else if (dir == 'a')
            confirmColl.transform.position = new Vector3(initialPositionX - bs.distSqaure, initialPositionY);
        else if (dir == 's')
            confirmColl.transform.position = new Vector3(initialPositionX, initialPositionY - bs.distSqaure);
        else if (dir == 'd')
            confirmColl.transform.position = new Vector3(initialPositionX + bs.distSqaure, initialPositionY);
        //Luego cancelamos movimiento si el pequeño detecta una colision de tipo tilemap collider
        if (confirmColl.collides || (canMove && !confirmColl.cuadraVerde))
            cancelMove();
    }
    public void cancelMove()
    {
        //Debug.Log("Algo ha fallado");
        this.transform.position = new Vector2(initialPositionX, initialPositionY);
        moving = false;
        dir = ' ';
    }
    
    public IEnumerator desactivarAtaque()
    {
        yield return new WaitForSeconds(2);
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PJController pj = collision.gameObject.GetComponent<PJController>();
        if (pj != null && !pj.aliado)
        {
            pjEnemigo = pj;
        }
        else if (pj != null && pj.aliado)
        {
            pjAliado = pj;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Tilemap>() != null)
        {
            cancelMove();
        }
        PJController pj = collision.gameObject.GetComponent<PJController>();
        if (pj != null && !pj.aliado)
        {
            pjEnemigo = pj;
        }
        else if (pj != null && pj.aliado)
        {
            pjAliado = pj;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        PJController pj = collision.gameObject.GetComponent<PJController>();
        if (pj != null && !pj.aliado)
        {
            pjEnemigo = null;
        }
        else if (pj != null && pj.aliado)
        {
            pjAliado = null;
        }
    }

}
