﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class BattleSeleter : MonoBehaviour
{
    private char dir;
    public bool moving;
    private float initialPositionX;
    private float sumX;
    private float initialPositionY;
    private float sumY;

    public bool interactuando;

    public float distSqaure = 1f;
    public float speedSquare = 2.5f;

    private GameObject player;
    private Direction previousDirection = Direction.Down;

    private ConfirmColl confirmColl;
    public int contador = 0;

    private void Awake()
    {
        Application.targetFrameRate = 30;//fps del juego (battle selecter)
    }
    // Start is called before the first frame update
    void Start()
    {
        confirmColl = this.transform.GetChild(0).GetComponent<ConfirmColl>();
        dir = ' ';
        moving = false;
        player = GameObject.Find("Player");
        if (player != null)
        {
            previousDirection = player.GetComponent<PlayerController>().direccion;
            player.GetComponent<PlayerController>().cambiarLayer(-10);
            StartCoroutine(player.GetComponent<Teletransportador>().desvanecerPantallaNegra());
            player.GetComponent<PlayerController>().battling = true;
            player.SetActive(false);
        }
        interactuando = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("m"))
            GameObject.Find("BattleSystemManager").GetComponent<BattleSystemManager>().endBattle();

        //updateSprite();//actualizaba el sprite sin el animator
        if (!interactuando)
        {
            moveCasilla();//detecta wasd y se mueve de tile en tile 
        }

    }


    private void moveCasilla()
    {
        if (!moving)
        {
            if (Input.GetKey("w")) dir = 'w';
            if (Input.GetKey("a")) dir = 'a';
            if (Input.GetKey("s")) dir = 's';
            if (Input.GetKey("d")) dir = 'd';
            if (Input.anyKey) moving = true;
            if (moving)
            {
                initialPositionX = this.transform.position.x;
                initialPositionY = this.transform.position.y;
            }
        }
        else
        {
            predictCollision();
            if (moving) //si no detecta una collision
            {
                Vector3 targetPosition = new Vector3(initialPositionX, initialPositionY); //inicializo objetivo
                if (dir == 'w')
                    targetPosition = new Vector3(initialPositionX, initialPositionY + distSqaure);
                else if (dir == 'a')
                    targetPosition = new Vector3(initialPositionX - distSqaure, initialPositionY);
                else if (dir == 's')
                    targetPosition = new Vector3(initialPositionX, initialPositionY - distSqaure);
                else if (dir == 'd')
                    targetPosition = new Vector3(initialPositionX + distSqaure, initialPositionY);
                //Direccion en la que se mueve (1,0)=derecha (-1,0)=izquierda (0,1)=arriba (0,-1)=abajo //0 1 aparecen por el normalized
                Vector3 moveDir = (targetPosition - transform.position).normalized;//tambien podria ser por ejemplo derecha y abajo --> (1,-1)
                //Para adaptarse a la velocidad del ordenador utilizamos Time.deltaTime
                float moveAmount = speedSquare * Time.deltaTime;
                //A partir de la velocidad calculada la reajustamos con Mathf.Clamp(numero, min, max),
                //si el movimiento sobrepasa el objetivo se reajustara al maximo dado por parametros en este caso la diferencia de distancia al objetivo
                //cuando sea necesario utilizar el maximo se hará la última suma a la posicion
                moveAmount = Mathf.Clamp(moveAmount, 0f, Vector3.Distance(transform.position, targetPosition));
                //sumar cantidad de movimiento
                transform.position += moveDir * moveAmount;
                if (Vector3.Distance(transform.position, targetPosition) == 0.0f)//diferencia entre el battleSelecter y el objetivo
                {
                    contador++;//suma cada vez que se mueve a otra casilla, si colisiona no se suma
                    moving = false;
                    dir = ' ';
                    confirmColl.transform.position = this.transform.position;// new Vector3(this.transform.position.x, initialPositionY + distSqaure);
                }
            }
            //Todo esto lo hacia antes de descubir el Mathf.Clamp(numero, min, max)
            /* 
            if (Mathf.Abs(this.transform.position.x - initialPositionX) == distSqaure || Mathf.Abs(this.transform.position.y - initialPositionY) == distSqaure)
            {
                moving = false;
                dir = ' ';
                distSqaure = (Mathf.Abs(distSqaure));
            }
            else if (Mathf.Abs(this.transform.position.x - initialPositionX) > distSqaure || Mathf.Abs(this.transform.position.y - initialPositionY) > distSqaure)
            {
                if (dir == 'w') this.transform.position = new Vector2(initialPositionX, initialPositionY + distSqaure);
                if (dir == 'a') this.transform.position = new Vector2(initialPositionX - distSqaure, initialPositionY);
                if (dir == 's') this.transform.position = new Vector2(initialPositionX, initialPositionY - distSqaure);
                if (dir == 'd') this.transform.position = new Vector2(initialPositionX + distSqaure, initialPositionY);
                moving = false;
                dir = ' ';
                distSqaure = (Mathf.Abs(distSqaure));
            }*/
        }
    }

    private void predictCollision()
    {
        //Primero movemos el selecter pequeño a la siguiente casilla
        if (dir == 'w')
            confirmColl.transform.position = new Vector3(initialPositionX, initialPositionY + distSqaure);
        else if (dir == 'a')
            confirmColl.transform.position = new Vector3(initialPositionX - distSqaure, initialPositionY);
        else if (dir == 's')
            confirmColl.transform.position = new Vector3(initialPositionX, initialPositionY - distSqaure);
        else if (dir == 'd')
            confirmColl.transform.position = new Vector3(initialPositionX + distSqaure, initialPositionY);
        //Luego cancelamos movimiento si el pequeño detecta una colision de tipo tilemap collider
        if (confirmColl.collides)
            cancelMove();
    }

    public void cancelMove()
    {
        this.transform.position = new Vector2(initialPositionX, initialPositionY);
        moving = false;
        dir = ' ';
    }
    public void endBattle()
    {
        if (player != null && SceneManager.GetActiveScene().buildIndex >= 1)
        {
            Debug.Log("hola");
            player.SetActive(true);
            player.GetComponent<PlayerController>().cambiarDireccion(previousDirection);
            player.GetComponent<Teletransportador>().TransportarEscena(player.transform.position, previousDirection, 0);
            player.GetComponent<PlayerController>().battling = true;
        }
        //else
        //SceneManager.LoadScene(0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Tilemap>() != null)
        {
            cancelMove();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {

    }
}
