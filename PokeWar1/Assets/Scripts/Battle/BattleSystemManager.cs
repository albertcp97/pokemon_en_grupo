﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleSystemManager : MonoBehaviour
{

    public GameObject new_aliado;
    public GameObject new_enemigo;

    public AudioClip cancion;

    //private bool miTurno;
    private int countTurn;

    private GameObject aliados;
    private GameObject enemigos;

    private PlayerController player;
    private GameObject eventos;
    private GameObject menu;

    private PJController protagonista;
    private PJController rival;

    private ActionManager actionManager;

    private bool movingEnemies;

    private void Awake()
    {
        //miTurno = true;

        aliados = GameObject.Find("Aliados");
        enemigos = GameObject.Find("Oponentes");

        player = GameObject.Find("Player").GetComponent<PlayerController>();
        eventos = GameObject.Find("Eventos");
        menu = GameObject.Find("Menu");
        eventos.SetActive(false);
        menu.SetActive(false);

        actionManager = GameObject.Find("ActionManager").GetComponent<ActionManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<AudioSource>().clip = cancion;
        this.GetComponent<AudioSource>().Play();
        //Rellenar aliados (listaPokemon del personaje)
        //Rellenar enemigos (Entrenador y listaPokemon del entrenador)
        protagonista = GameObject.Find("Protagonista").GetComponent<PJController>();
        rival = GameObject.Find("Rival").GetComponent<PJController>();
        protagonista.entrenador = player.entrenador;
        rival.entrenador = player.trainerEnemigo;
        //cambiar grafico rival
        Debug.Log(rival.entrenador.nombreGrafico);
        GameObject.Find("Rival").GetComponent<Animator>().Play(rival.entrenador.nombreGrafico);
        //instanciar pokemon
        rodear(protagonista.entrenador, 0);
        rodear(rival.entrenador, 1);
    }
    private void FixedUpdate()
    {
        if(!this.GetComponent<AudioSource>().isPlaying)
            this.GetComponent<AudioSource>().Play();
    }
    private void rodear(Entrenador e, int i)
    {
        float posX = GameObject.Find("Protagonista").transform.position.x;
        float posY = GameObject.Find("Protagonista").transform.position.y;
        GameObject objeto = new_aliado;
        GameObject padre = aliados;
        if (i == 1)
        {
            objeto = new_enemigo;
            padre = enemigos;
            posX = GameObject.Find("Rival").transform.position.x;
            posY = GameObject.Find("Rival").transform.position.y;
        }
        if (e.listaPokemon.Count == 4)
            instanciarPokemon(objeto, padre, e.listaPokemon[3], posX-2.0f, posY);//IZQUIERDA
        if (e.listaPokemon.Count >= 3)
            instanciarPokemon(objeto, padre, e.listaPokemon[2], posX, posY - 2);//ABAJO
        if (e.listaPokemon.Count >= 2)
            instanciarPokemon(objeto, padre, e.listaPokemon[1], posX+2, posY);//DERECHA
        if (e.listaPokemon.Count >= 1)
            instanciarPokemon(objeto, padre, e.listaPokemon[0], posX, posY+2);//ARRIBA
        if (e.listaPokemon.Count < 1 || e.listaPokemon.Count > 4)
            Debug.LogError("Error, numero de pokemon invalido");//ERROR
    }
    private void instanciarPokemon(GameObject objeto, GameObject padre, Pokemon poke, float posX, float posY)
    {
        GameObject Pokemon = Instantiate(objeto);
        Pokemon.transform.position = new Vector2(posX, posY);
        Pokemon.name = poke.nombre;
        Pokemon.GetComponent<Animator>().Play(poke.nombre);
        Pokemon.GetComponent<PJController>().isPokemon = true;
        Pokemon.GetComponent<PJController>().pokemon = poke;
        Pokemon.transform.parent = padre.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (acabado())
        {
            endBattle();
        }
        if (!movingEnemies)
        {
            if (checkAllMoved(aliados) && !checkAllMoved(enemigos))
            {
                moveAllEnemies();
            }
        }
        if (checkAllMoved(enemigos))
        {
            movingEnemies = false;
            GameObject.Find("ActionManager").GetComponent<ActionManager>().FuckGoBack();
            disableMoved(aliados);
            disableMoved(enemigos);
        }
    }

    private void moveAllEnemies()
    {
        movingEnemies = true;
        GameObject protagonista = GameObject.Find("Protagonista");
        //inteligencia artifical de acercarse(solo 1 casilla) o atacar a 3 casillas de distancia
        foreach (Transform child in enemigos.transform) //navego entre los objetos hijos
        {
            PJController pj = child.GetComponent<PJController>();
            if (pj != null)
            {
                StartCoroutine(moverpj(pj));
            }
        }
    }
    public IEnumerator moverpj(PJController pj)
    {
        bool atacado = false;
        if (pj.isPokemon)
        {
            crearCasillas(pj.transform.position.x, pj.transform.position.y, 3);
            yield return new WaitForSeconds(0.5f);
            foreach (Transform casilla in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
            {
                Destruirloquesea componente = casilla.GetComponent<Destruirloquesea>();
                Debug.Log(componente);
                Debug.Log(componente.pjAliado);
                if (componente != null && componente.detectaAliado)
                {
                    Movimiento ataqueUsado = pj.pokemon.movimientos[Random.Range(0, pj.pokemon.movimientos.Count - 1)];
                    Debug.Log(ataqueUsado.nombre);
                    actionManager.attackController.gameObject.SetActive(true);
                    actionManager.attackController.gameObject.transform.position= componente.pjAliado.transform.position;
                    actionManager.attackController.GetComponent<Animator>().Play(ataqueUsado.nombre);
                    
                    decimal dividendo = decimal.Divide(pj.pokemon.atk, 100);
                    double mult = ataqueUsado.poder*(double)dividendo;
                    componente.pjAliado.RecibirDano(ataqueUsado.poder+mult);
                    StartCoroutine(actionManager.attackController.desactivarAtaque());
                    atacado = true;
                    pj.movido = true;
                    break;
                }
            }
            yield return new WaitForSeconds(0.5f);
            foreach (Transform casilla in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
            {
                Destroy(casilla.gameObject);
            }
        }
        if (!atacado)
        {
            float diferenciaX = protagonista.transform.position.x - pj.transform.position.x;
            float diferenciaY = protagonista.transform.position.y - pj.transform.position.y;

            if (Mathf.Abs(diferenciaX) >= Mathf.Abs(diferenciaY))
            {
                if (diferenciaX < 0)
                    pj.moveDireccion('a');//left
                else
                    pj.moveDireccion('d');//right
            }
            else
            {
                if (diferenciaY < 0)
                    pj.moveDireccion('s');//down
                else
                    pj.moveDireccion('w');//up
            }
        }

    }
    public void crearCasillas(float x, float y, int m)
    {
        GameObject newCuadra = Instantiate(actionManager.cuadro);
        newCuadra.name = "Cuadra";
        newCuadra.transform.position = new Vector2(x, y);
        foreach (Transform child in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
        {
            if (child.transform.position == newCuadra.transform.position)
                Destroy(child.gameObject);
        }
        newCuadra.transform.parent = GameObject.Find("padreCuadros").transform;
        if (m > 0)
        {
            crearCasillas(x - 1, y, m - 1);//izquierda
            crearCasillas(x + 1, y, m - 1);//derecha

            crearCasillas(x, y - 1, m - 1);//abajo
            crearCasillas(x, y + 1, m - 1);//arriba
        }
    }

    private bool checkAllMoved(GameObject padre)
    {
        int nPersonajes = 0;
        int nMovidos = 0;
        foreach (Transform child in padre.transform) //navego entre los objetos hijos
        {
            PJController pj = child.GetComponent<PJController>();
            if (pj != null)
            {
                nPersonajes++;
                if (pj.movido)
                    nMovidos++;
            }
        }
        if (nPersonajes == nMovidos)
            return true;
        return false;
    }
    private void disableMoved(GameObject padre)
    {
        foreach (Transform child in padre.transform) //navego entre los objetos hijos
        {
            PJController pj = child.GetComponent<PJController>();
            if (pj != null)
            {
                pj.movido = false;
            }
        }
    }
    private bool acabado()
    {
        bool existeProtagonista = false;
        int nAliados = 0;
        foreach (Transform child in aliados.transform) //navego entre los objetos hijos
        {
            PJController pj = child.GetComponent<PJController>();
            if (pj != null)
            {
                nAliados++;
                if (pj.gameObject.name.Equals("Protagonista"))
                    existeProtagonista = true;
            }
        }
        bool existeRival = false;
        int nEnemigos = 0;
        foreach (Transform child in enemigos.transform) //navego entre los objetos hijos
        {
            PJController pj = child.GetComponent<PJController>();
            if (pj != null)
            {
                nEnemigos++;
                if (pj.gameObject.name.Equals("Rival"))
                    existeRival = true;
            }
        }
        if (existeProtagonista && existeRival && nAliados>1 && nEnemigos>1)
            return false;
        else if ((existeProtagonista && !existeRival) || (nAliados>1 && nEnemigos==1)){
            if (rival.entrenador.expdada>=protagonista.entrenador.expnec){
                protagonista.entrenador.subirNivel();
            }
            else{
                protagonista.entrenador.expnec-=rival.entrenador.expdada;
            }
            return true;
        }
        else
            return true;
    }
    public void endBattle()
    {
        Debug.Log("se acabo la batalla");
        this.GetComponent<AudioSource>().Stop();
        eventos.SetActive(true);
        menu.SetActive(true);
        player.gameObject.SetActive(true);
        //SceneManager.LoadScene(0);
        player.GetComponent<Teletransportador>().TransportarEscena(player.transform.position, player.direccion, 0);
        //player.battling = false;
        player.eventing = false;
        player.gameObject.SetActive(true);

    }
}
