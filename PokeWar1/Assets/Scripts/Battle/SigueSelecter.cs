﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SigueSelecter : MonoBehaviour
{
    BattleSeleter bs = null;
    int distancia = 3;
    // Start is called before the first frame update
    void Start()
    {
        bs = GameObject.Find("Selecter").GetComponent<BattleSeleter>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float diferenciaX = this.transform.position.x - bs.transform.position.x;
        float diferenciaY = this.transform.position.y - bs.transform.position.y;
        if (diferenciaX > distancia)
            this.transform.position = new Vector3(bs.transform.position.x + distancia, this.transform.position.y, -10);
        else if (diferenciaX < -distancia)
            this.transform.position = new Vector3(bs.transform.position.x - distancia, this.transform.position.y, -10);
        else if (diferenciaY > distancia)
            this.transform.position = new Vector3(this.transform.position.x, bs.transform.position.y + distancia, -10);
        else if (diferenciaY < -distancia)
            this.transform.position = new Vector3(this.transform.position.x, bs.transform.position.y - distancia, -10);
    }
}
