﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ActionManager : MonoBehaviour
{
    public GameObject cuadra;
    public GameObject cuadro;
    private EventSystem eventSys;
    private GameObject prota;

    private GameObject detectCuadra = null;
    float centroX;
    float centroY;

    public GameObject btMoverCancelar = null;
    public GameObject btMover = null;
    public GameObject btAtacar = null;
    public GameObject btPasarTurno = null;
    public GameObject btAtras = null;
    public GameObject btAttack1 = null;
    public GameObject btAttack2 = null;
    public GameObject btAttack3 = null;
    public GameObject btAttackCancel = null;
    
    public AttackController attackController;

    private void Awake()
    {
        btMoverCancelar = GameObject.Find("ButtonMoverCancelar");
        btMover = GameObject.Find("ButtonMover");
        btAtacar = GameObject.Find("ButtonAtacar");
        btPasarTurno = GameObject.Find("ButtonPasarTurno");
        btAtras = GameObject.Find("ButtonAtras");
        btAttack1 = GameObject.Find("ButtonAtacar1");
        btAttack2 = GameObject.Find("ButtonAtacar2");
        btAttack3 = GameObject.Find("ButtonAtacar3");
        btAttackCancel = GameObject.Find("ButtonAtacarCancelar");

        attackController = GameObject.Find("AttackAnim").GetComponent<AttackController>();
        attackController.gameObject.SetActive(false);
    }
    private void Start()
    {
        eventSys = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        prota = GameObject.FindGameObjectWithTag("Prota");

        detectCuadra = GameObject.Find("DetectCuadro");
    }
    private void Update()
    {
        eventSys.SetSelectedGameObject(null);
    }
    public void Atacar(int nAtaque)
    {
        PJController pjController = GameObject.Find("Selecter").GetComponent<PlayerSeleter>().pjController;
        if (pjController.isPokemon)
        {
            float x = pjController.gameObject.transform.position.x;
            float y = pjController.gameObject.transform.position.y;
            centroX = x;
            centroY = y;
            //inicializar ataque
            Movimiento ataqueSeleccionado = pjController.pokemon.movimientos[nAtaque-1];//TO DO asignara la animacion y el rango de ataque
            attackController.gameObject.SetActive(true);
            attackController.GetComponent<Animator>().Play(ataqueSeleccionado.nombre);
            attackController.pjAtacante = pjController;
            attackController.ataque = ataqueSeleccionado;
            attackController.transform.position = new Vector2(x, y);
            int m = ataqueSeleccionado.alcance;
            llenarCasilla(x, y, m, 2);//funcion recursiva
                                      //Desactivar botones
            btAttack1.SetActive(false);
            btAttack2.SetActive(false);
            btAttack3.SetActive(false);
            btAttackCancel.SetActive(true);
            //Mover ataque
            attackController.canMove = true;
        }
    }
    public void Mover()
    {
        PJController pjController = GameObject.Find("Selecter").GetComponent<PlayerSeleter>().pjController;
        float x = pjController.gameObject.transform.position.x;
        float y = pjController.gameObject.transform.position.y;
        int m = pjController.mov;
        centroX = x;
        centroY = y;
        llenarCasilla(x, y, m, 1);//funcion recursiva

        btMover.SetActive(false);
        btAtacar.SetActive(false);
        btPasarTurno.SetActive(false);
        btAtras.SetActive(false);
        btMoverCancelar.SetActive(true);

        pjController.canMove = true;
        //GameObject.Find("Selecter").GetComponent<BattleSeleter>().interactuando = true;
    }
    public void cancelarMover()
    {
        foreach (Transform child in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
        {
            Destroy(child.gameObject);
        }
        attackController.canMove = false;
        attackController.gameObject.SetActive(false);
        PJController pjController = GameObject.Find("Selecter").GetComponent<PlayerSeleter>().pjController;
        pjController.transform.position = new Vector2(centroX, centroY);
        pjController.canMove = false;

        FuckGoBack();
    }

    public void llenarCasilla(float x, float y, int m, int color)
    {
        GameObject newCuadra;
        if (color == 1)
        {
            newCuadra = Instantiate(cuadra);
        }
        else
        {
            newCuadra = Instantiate(cuadro);
        }
        newCuadra.name = "Cuadra";
        newCuadra.transform.position = new Vector2(x, y);
        foreach (Transform child in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
        {
            if (child.transform.position == newCuadra.transform.position)
                Destroy(child.gameObject);
        }
        newCuadra.transform.parent = GameObject.Find("padreCuadros").transform;
        if (m > 0)
        {
            centroX -= 1;
            llenarCasilla(x - 1, y, m - 1, color);//izquierda
            centroX += 2;
            llenarCasilla(x + 1, y, m - 1, color);//derecha
            centroX -= 1;

            centroY -= 1;
            llenarCasilla(x, y - 1, m - 1, color);//abajo
            centroY += 2;
            llenarCasilla(x, y + 1, m - 1, color);//arriba
            centroY -= 1;
        }
    }

    public void Atack()
    {
        PJController pjController = GameObject.Find("Selecter").GetComponent<PlayerSeleter>().pjController;
        if (pjController.isPokemon)
        {
            btMover.SetActive(false);
            btAtacar.SetActive(false);
            btPasarTurno.SetActive(false);
            btAtras.SetActive(false);
            btMoverCancelar.SetActive(false);

            btAttack1.SetActive(true);
            btAttack2.SetActive(true);
            btAttack3.SetActive(true);
            btAttackCancel.SetActive(true);
            //Escribir nombre ataque
            btAttack1.GetComponentInChildren<Text>().text = pjController.pokemon.movimientos[0].nombre;
            btAttack2.GetComponentInChildren<Text>().text = pjController.pokemon.movimientos[1].nombre;
            btAttack3.GetComponentInChildren<Text>().text = pjController.pokemon.movimientos[2].nombre;

            //TO DO cambiar el nombre de los ataques segun personaje
            //btAttack1.GetComponentInChildren<Text>().text = ;

            GameObject.Find("Selecter").GetComponent<BattleSeleter>().interactuando = true;
        }
        else if(pjController.entrenador.getInventario().Count > 0)
        {
            float x = pjController.gameObject.transform.position.x;
            float y = pjController.gameObject.transform.position.y;
            centroX = x;
            centroY = y;
            //inicializar ataque
            attackController.gameObject.SetActive(true);
            attackController.GetComponent<Animator>().Play("Curar");
            attackController.pjAtacante = pjController;
            attackController.transform.position = new Vector2(x, y);
            int m = pjController.entrenador.mov;
            llenarCasilla(x, y, m, 2);//funcion recursiva
            //Desactivar botones
            btMover.SetActive(false);
            btAtacar.SetActive(false);
            btPasarTurno.SetActive(false);
            btAtras.SetActive(false);
            btAttackCancel.SetActive(true);
            //Mover ataque/curar
            attackController.canMove = true;
        }


    }
    public void SkipTurn()
    {
        PJController pjController = GameObject.Find("Selecter").GetComponent<PlayerSeleter>().pjController;
        pjController.movido = true;
        FuckGoBack();
    }
    public void FuckGoBack()
    {
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().dale = false;
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().pjController = null;
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().ataque = false;

        GameObject.Find("Selecter").GetComponent<BattleSeleter>().interactuando = false;

        btMover.SetActive(false);
        btAtacar.SetActive(false);
        btPasarTurno.SetActive(false);
        btAtras.SetActive(false);
        btMoverCancelar.SetActive(false);
        btAttack1.SetActive(false);
        btAttack2.SetActive(false);
        btAttack3.SetActive(false);
        btAttackCancel.SetActive(false);

        eventSys.SetSelectedGameObject(null);
    }


}
