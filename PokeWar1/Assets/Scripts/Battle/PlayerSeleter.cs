﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;



public class PlayerSeleter : MonoBehaviour
{
    public bool dale = false;
    public PJController pjController = null;

    public GameObject btMoverCancelar;
    public GameObject btMover;
    public GameObject btAtacar;
    public GameObject btPasarTurno;
    public GameObject btAtras;
    public GameObject btAttack1;
    public GameObject btAttack2;
    public GameObject btAttack3;
    public GameObject btAttackCancel;
    public bool ataque = false;
    private EventSystem even;

    private void Awake()
    {
        btMoverCancelar = GameObject.Find("ButtonMoverCancelar");
        btMover = GameObject.Find("ButtonMover");
        btAtacar = GameObject.Find("ButtonAtacar");
        btPasarTurno = GameObject.Find("ButtonPasarTurno");
        btAtras = GameObject.Find("ButtonAtras");
        btAttack1 = GameObject.Find("ButtonAtacar1");
        btAttack2 = GameObject.Find("ButtonAtacar2");
        btAttack3 = GameObject.Find("ButtonAtacar3");
        btAttackCancel = GameObject.Find("ButtonAtacarCancelar");
    }

    void Start()
    {
        btMoverCancelar.SetActive(false);
        btMover.SetActive(false);
        btAtacar.SetActive(false);
        btPasarTurno.SetActive(false);
        btAtras.SetActive(false);
        btAttack1.SetActive(false);
        btAttack2.SetActive(false);
        btAttack3.SetActive(false);
        btAttackCancel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (dale && !this.gameObject.GetComponent<BattleSeleter>().interactuando)
        {
            if (Input.GetKey(KeyCode.Space) && !this.gameObject.GetComponent<BattleSeleter>().moving)
            {
                this.gameObject.GetComponent<BattleSeleter>().interactuando = true;
                btMover.SetActive(true);
                btAtacar.SetActive(true);
                if (this.pjController.isPokemon)
                    btAtacar.GetComponentInChildren<Text>().text = "Atacar";
                else
                {
                    if(pjController.entrenador.getInventario().Count > 0)
                        btAtacar.GetComponentInChildren<Text>().text = "Curar";
                    else
                        btAtacar.GetComponentInChildren<Text>().text = "(Vacio)";
                }
                btPasarTurno.SetActive(true);
                btAtras.SetActive(true);
                btMoverCancelar.SetActive(false);
            }
            //ataque movido a ActionManager
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PJController pj = collision.gameObject.GetComponent<PJController>();
        if (pj != null && !pj.movido)
        {
            if (pj.aliado)
                dale = true;
            pjController = pj;
        }
    }
    private void OnTriggerTrigger2D(Collider2D collision)
    {
        PJController pj = collision.gameObject.GetComponent<PJController>();
        if (pj != null && !pj.movido)
        {
            if (pj.aliado)
                dale = true;
            pjController = pj;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        PJController pj = collision.gameObject.GetComponent<PJController>();
        if ((pj != null) && !this.gameObject.GetComponent<BattleSeleter>().interactuando)
        {
            dale = false;
            pjController = null;
        }
    }

}
