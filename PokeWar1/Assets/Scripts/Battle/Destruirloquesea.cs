﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destruirloquesea : MonoBehaviour
{
    public bool detectaAliado = false;
    public PJController pjAliado = null;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PJController pj = collision.gameObject.GetComponent<PJController>();
        if (pj != null && pj.aliado)
        {
            detectaAliado = true;
            pjAliado = pj;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        PJController pj = collision.gameObject.GetComponent<PJController>();
        if (pj != null && pj.aliado)
        {
            //Debug.Log("te voy a atacar a " + pj.gameObject.name);
            detectaAliado = true;
            pjAliado = pj;
        }
    }
}
