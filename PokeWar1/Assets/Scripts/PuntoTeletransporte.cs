﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntoTeletransporte : MonoBehaviour
{

    public PuntoTeletransporte destino;

    public bool mismaDireccion = false;
    public Direction nuevaDireccion;
    public bool moveForward = false;
    public AudioClip enterAudio;

    private bool canTeleport;
    private PlayerController personaje = null;
    private AudioSource SE = null;
    
    private void Start()
    {
        canTeleport = true;
        personaje = GameObject.Find("Player").gameObject.GetComponent<PlayerController>();
        this.gameObject.AddComponent<AudioSource>();
        SE = this.gameObject.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name.Equals("Player") && personaje != null)
        {
            if (destino != null && canTeleport)
            {
                SE.PlayOneShot(enterAudio);
                if (mismaDireccion) nuevaDireccion = personaje.direccion;
                destino.canTeleport = false;
                this.canTeleport = false;
                Teletransportador t = personaje.gameObject.GetComponent<Teletransportador>();
                t.Transportar(destino.transform.position, nuevaDireccion, moveForward);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController personaje = collision.gameObject.GetComponent<PlayerController>();
        if (personaje != null && !canTeleport)
        {
            canTeleport = true;
        }
    }
}
