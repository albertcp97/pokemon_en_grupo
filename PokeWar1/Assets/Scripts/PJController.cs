﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PJController : MonoBehaviour
{
    public bool aliado = true;

    public bool isPokemon = false;
    public Entrenador entrenador = null;
    public Pokemon pokemon = null;

    public string nom = "nuevo";
    public int mov = 5;
    public bool canMove = false;
    public bool movido = false;
    
    private char dir;
    private bool moving;
    private float initialPositionX;
    private float initialPositionY;
    private ConfirmColl confirmColl;
    private SpriteRenderer basePJ;
    private Color colorBase;
    public int vidaMax;
    public int vidaAct;

    public GameObject[] VidaM;
    

    // Start is called before the first frame update
    private void Start()
    { 
        canMove = false;
        foreach (Transform child in this.transform) //navego entre los objetos hijos
        {
            if (child.gameObject.GetComponent<ConfirmColl>() != null)
                this.confirmColl = child.gameObject.GetComponent<ConfirmColl>();
            if (child.gameObject.name.Equals("Base"))
                basePJ = child.GetComponent<SpriteRenderer>();
        }
        colorBase = basePJ.color;
        if (confirmColl == null)
        {
            Debug.Log("Error te falta poner el objeto CheckColl en un personaje");
        }

        
        vidaMax = 100;
        vidaAct = 100;
        //pintarVida();
        //RecibirDano(20);

    }

    private void pintarVida()
    {
        float posX=this.gameObject.transform.position.x;
        float posY = this.gameObject.transform.position.y + 1.3f;
        
          
        GameObject newVidaA = Instantiate(VidaM[0]);
       
        GameObject newVidaM = Instantiate(VidaM[1]);
            newVidaA.transform.position = new Vector2(posX, posY);

        newVidaM.transform.position = new Vector2(posX, posY);
        
    }

    private void FixedUpdate()
    {
        this.GetComponent<SpriteRenderer>().sortingOrder = (int)(100-this.transform.position.y);
        foreach (Transform child in transform)
        {
            child.GetComponent<SpriteRenderer>().sortingOrder = (int)(99 - this.transform.position.y);
        }
    }
    // Update is called once per frame
    private void Update()
    {
        if (canMove)
        {
            moveCasilla();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                foreach (Transform child in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
                {
                    Destroy(child.gameObject);
                }
                canMove = false;
                movido = true;
                GameObject.Find("ActionManager").GetComponent<ActionManager>().FuckGoBack();
            }
        }
        if (movido)
        {
            Color c = basePJ.color;
            c.r = 50; c.g = 50; c.b = 50; c.a = 125;
            basePJ.color = c;
        }
        else
        {
            basePJ.color = colorBase;
        }
    }
    public void RecibirDano(double dano)
    {
        if (this.isPokemon){
            decimal dividendo = decimal.Divide(this.pokemon.def, 100);
            dano-=dano*(double)dividendo;
        }
        else {
            decimal dividendo = decimal.Divide(this.entrenador.def, 100);
            dano-=dano*(double)dividendo;
        }
        Debug.Log("Recibe " + dano +" de daño");
        if (vidaAct <= dano)
        {
            vidaAct = 0;
            Destroy(this.gameObject);
            Destroy(this);
        }
        else
        {
            int danoint = Convert.ToInt32(dano);
            vidaAct -= danoint;
        }
    }
    public void CurarVida(int cura)
    {
        Debug.Log("Curando "+ cura +" de vida");
        if(vidaAct+cura >= vidaMax)
        {
            vidaAct = vidaMax;
        }
        else
        {
            vidaAct += cura;
        }
    }
        private IEnumerator perderVida(int resta)
    {
        while(vidaAct != 0)
        {
            yield return null;
        }

    }
    private void moveCasilla()
    {
        if (!moving)
        {
            if (Input.GetKey("w")) dir = 'w';
            if (Input.GetKey("a")) dir = 'a';
            if (Input.GetKey("s")) dir = 's';
            if (Input.GetKey("d")) dir = 'd';
            if (Input.anyKey) moving = true;
            if (moving)
            {
                initialPositionX = this.transform.position.x;
                initialPositionY = this.transform.position.y;
            }
        }
        else
        {
            predictCollision(dir);
            BattleSeleter bs = GameObject.Find("Selecter").GetComponent<BattleSeleter>();
            if (moving) //si no detecta una collision
            {
                Vector3 targetPosition = new Vector3(initialPositionX, initialPositionY); //inicializo objetivo
                if (dir == 'w')
                    targetPosition = new Vector3(initialPositionX, initialPositionY + bs.distSqaure);
                else if (dir == 'a')
                    targetPosition = new Vector3(initialPositionX - bs.distSqaure, initialPositionY);
                else if (dir == 's')
                    targetPosition = new Vector3(initialPositionX, initialPositionY - bs.distSqaure);
                else if (dir == 'd')
                    targetPosition = new Vector3(initialPositionX + bs.distSqaure, initialPositionY);
                //Direccion en la que se mueve (1,0)=derecha (-1,0)=izquierda (0,1)=arriba (0,-1)=abajo //0 1 aparecen por el normalized
                Vector3 moveDir = (targetPosition - transform.position).normalized;//tambien podria ser por ejemplo derecha y abajo --> (1,-1)
                //Para adaptarse a la velocidad del ordenador utilizamos Time.deltaTime
                float moveAmount = bs.speedSquare * Time.deltaTime;
                //A partir de la velocidad calculada la reajustamos con Mathf.Clamp(numero, min, max),
                //si el movimiento sobrepasa el objetivo se reajustara al maximo dado por parametros en este caso la diferencia de distancia al objetivo
                //cuando sea necesario utilizar el maximo se hará la última suma a la posicion
                moveAmount = Mathf.Clamp(moveAmount, 0f, Vector3.Distance(transform.position, targetPosition));
                //sumar cantidad de movimiento
                transform.position += moveDir * moveAmount;
                if (Vector3.Distance(transform.position, targetPosition) == 0.0f)//diferencia entre el battleSelecter y el objetivo
                {
                    moving = false;
                    dir = ' ';
                    confirmColl.transform.position = this.transform.position;// new Vector3(this.transform.position.x, initialPositionY + distSqaure);
                }
            }
        }
    }
    private void predictCollision(char dir)
    {
        BattleSeleter bs = GameObject.Find("Selecter").GetComponent<BattleSeleter>();
        //Primero movemos el selecter pequeño a la siguiente casilla
        if (dir == 'w')
            confirmColl.transform.position = new Vector3(initialPositionX, initialPositionY + bs.distSqaure);
        else if (dir == 'a')
            confirmColl.transform.position = new Vector3(initialPositionX - bs.distSqaure, initialPositionY);
        else if (dir == 's')
            confirmColl.transform.position = new Vector3(initialPositionX, initialPositionY - bs.distSqaure);
        else if (dir == 'd')
            confirmColl.transform.position = new Vector3(initialPositionX + bs.distSqaure, initialPositionY);
        //Luego cancelamos movimiento si el pequeño detecta una colision de tipo tilemap collider
        if (confirmColl.collides || (canMove && !confirmColl.cuadraVerde) || confirmColl.pjCollision)
            cancelMove();
    }
    public void cancelMove()
    {
        //Debug.Log("Algo ha fallado");
        this.transform.position = new Vector2(initialPositionX, initialPositionY);
        moving = false;
        dir = ' ';
    }

    public void moveDireccion(char dir)//float initialPositionX, float initialPositionY)
    {
        this.dir = dir;
        moving = true;
        initialPositionX = this.transform.position.x;
        initialPositionY = this.transform.position.y;
        StartCoroutine(starMoving());

    }

    private IEnumerator starMoving()//float initialPositionX, float initialPositionY)
    {
        while (moving) //si no detecta una collision
        {
            //moveCasilla();
            predictCollision(dir);
            BattleSeleter bs = GameObject.Find("Selecter").GetComponent<BattleSeleter>();
            if (moving) //si no detecta una collision
            {
                Vector3 targetPosition = new Vector3(initialPositionX, initialPositionY); //inicializo objetivo
                if (dir == 'w')
                    targetPosition = new Vector3(initialPositionX, initialPositionY + bs.distSqaure);
                else if (dir == 'a')
                    targetPosition = new Vector3(initialPositionX - bs.distSqaure, initialPositionY);
                else if (dir == 's')
                    targetPosition = new Vector3(initialPositionX, initialPositionY - bs.distSqaure);
                else if (dir == 'd')
                    targetPosition = new Vector3(initialPositionX + bs.distSqaure, initialPositionY);
                //Direccion en la que se mueve (1,0)=derecha (-1,0)=izquierda (0,1)=arriba (0,-1)=abajo //0 1 aparecen por el normalized
                Vector3 moveDir = (targetPosition - transform.position).normalized;//tambien podria ser por ejemplo derecha y abajo --> (1,-1)
                //Para adaptarse a la velocidad del ordenador utilizamos Time.deltaTime
                float moveAmount = bs.speedSquare * Time.deltaTime;
                //A partir de la velocidad calculada la reajustamos con Mathf.Clamp(numero, min, max),
                //si el movimiento sobrepasa el objetivo se reajustara al maximo dado por parametros en este caso la diferencia de distancia al objetivo
                //cuando sea necesario utilizar el maximo se hará la última suma a la posicion
                moveAmount = Mathf.Clamp(moveAmount, 0f, Vector3.Distance(transform.position, targetPosition));
                //sumar cantidad de movimiento
                transform.position += moveDir * moveAmount;
                if (Vector3.Distance(transform.position, targetPosition) == 0.0f)//diferencia entre el battleSelecter y el objetivo
                {
                    moving = false;
                    dir = ' ';
                    confirmColl.transform.position = this.transform.position;// new Vector3(this.transform.position.x, initialPositionY + distSqaure);
                }
            }
            //yield return null;
            yield return new WaitForSeconds(0.02f);
        }
        this.movido = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Tilemap>() != null)
        {
            cancelMove();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {

    }

}
