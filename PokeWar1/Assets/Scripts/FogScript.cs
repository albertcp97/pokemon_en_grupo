﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FogScript : MonoBehaviour
{
    public AnimationClip[] animationClips;

    private bool active = true;

    private SpriteRenderer fogSpriteRen;
    private Image fogImagen;
    private Animator fogAnimator;

    private void Start()
    {
        fogSpriteRen = this.gameObject.GetComponent<SpriteRenderer>();
        fogImagen = this.gameObject.GetComponent<Image>();
        fogAnimator = this.gameObject.GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        if (active)
            fogImagen.sprite = fogSpriteRen.sprite;
        else if(fogImagen.sprite!=null)
            fogImagen.sprite = null;
    }
    public void changeFog(string s)
    {
        fogAnimator.SetBool("nieve", false);
        if (s.Equals("nieve"))
        {
            fogAnimator.SetBool(s, true);
            StartCoroutine(appear());
        }
        else if(active)
        {
            StartCoroutine(disappear());
        }
    }

    private IEnumerator appear()
    {
        active = true;
        Color c = fogImagen.color;
        c.a = 0.0f;
        fogImagen.color = c;
        while (c.a < 1)
        {
            fogImagen.color = c;
            c.a += 0.1f;
            yield return new WaitForSeconds(0.01f);
        }
    }
    private IEnumerator disappear()
    {
        Color c = fogImagen.color;
        c.a = 1.0f;
        fogImagen.color = c;
        while (c.a > 0)
        {
            fogImagen.color = c;
            c.a -= 0.1f;
            yield return new WaitForSeconds(0.01f);
        }
        active = false;
    }
}
