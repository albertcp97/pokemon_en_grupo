﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnicObject : MonoBehaviour
{
    //Este objeto nunca se borrara
    private static GameObject permanentObject = null;

    private void Awake()
    {
        if (permanentObject == null)
        {
            permanentObject = this.gameObject;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
            Destroy(this);
        }
    }
}
