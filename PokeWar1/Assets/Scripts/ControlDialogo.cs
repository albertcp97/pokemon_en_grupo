﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ControlDialogo : MonoBehaviour
{
    private Image imageEmoticon;
    private Image imageBox;
    private Image imageShown;
    private Image textBox;
    private Text dialogo;
    private PlayerController personaje;

    private Interactivo interactivo = null;
    private bool canTalk;
    private AudioSource newAudio = new AudioSource();
    private bool changingMusic;

    public AudioClip[] SEs;
    public AudioClip[] MEs;
    // Start is called before the first frame update
    void Start()
    {
        personaje = GameObject.Find("Player").gameObject.GetComponent<PlayerController>();
        changePosition();
        imageEmoticon = GameObject.Find("ImageEmoticon").GetComponent<Image>();
        imageBox = GameObject.Find("ImageBox").GetComponent<Image>();
        imageShown = GameObject.Find("ImageShown").GetComponent<Image>();
        textBox = GameObject.Find("TextBox").GetComponent<Image>();
        dialogo = GameObject.Find("Dialogo").GetComponent<Text>();
        imageEmoticon.gameObject.SetActive(false);
        imageBox.gameObject.SetActive(false);
        textBox.gameObject.SetActive(false);
        //this.gameObject.AddComponent<AudioSource>();
        newAudio = this.gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        changePosition();
        if (Input.GetKeyDown(KeyCode.Space) && canTalk && !personaje.interacting && interactivo.tipoInteraccion == Interactivo.TipoInteraccion.Pulsar)
        {
            if (interactivo.gameObject.GetComponent<NPC>() != null)
                interactivo.gameObject.GetComponent<NPC>().mirarJugador();
            interactivo.Interactuar();
            //procesarTextos(interactivo.dialogos);
        }
    }

    public void showEmoticon(string s)
    {
        playSE(s);
        StartCoroutine(hideEmoticon(s));
    }
    private IEnumerator hideEmoticon(string s)
    {
        while (imageEmoticon.gameObject.activeInHierarchy)
        {
            yield return null;
        }
        imageEmoticon.gameObject.SetActive(true);
        imageEmoticon.GetComponent<Animator>().Play(s);
        yield return new WaitForSeconds(2);
        imageEmoticon.gameObject.SetActive(false);
    }
    public void showImage(string s)
    {
        imageBox.gameObject.SetActive(true);
        Sprite newImage = Resources.Load<Sprite>("Imagenes/"+s);
        imageShown.sprite = newImage;
    }
    public void stopAudio()
    {
        newAudio.Stop();
    }
    public void resetAudio()
    {
        newAudio.Stop();
        newAudio.Play();
    }
    public void playSE(string s)
    {
        AudioClip newClip = Resources.Load<AudioClip>("Audio/SoundEffects/" + s);
        //Debug.Log(newClip);
        newAudio.PlayOneShot(newClip);
    }
    public void playME(string s)
    {
        AudioClip newClip = Resources.Load<AudioClip>("Audio/Music/" + s);
        //Debug.Log(newClip);
        if(newClip!=null && newAudio.clip != newClip)//si ha encontrado el clip y no es el mismo al que se esta reproduciendo
        {
            StartCoroutine(touchVolumen(newClip));
        }
    }

    private IEnumerator touchVolumen(AudioClip newClip)
    {
        while (changingMusic)//espera a que acabe la anterior CoRoutine que cambia musica
        {
            yield return null;
        }
        changingMusic = true;//define que esta cambiando la música
        if (newAudio.clip != null)//decrece musica si se esta reproduciendo
        {
            newAudio.volume = 1;
            while (newAudio.volume > 0)
            {
                newAudio.volume -= 0.1f;
                yield return new WaitForSeconds(0.2f);
                //yield return null;
            }
        }
        newAudio.clip = newClip;
        if (newAudio.clip != null)
            newAudio.Play();
        while (newAudio.volume < 1)
        {
            newAudio.volume += 0.1f;
            yield return new WaitForSeconds(0.2f);
            //yield return null;
        }
        changingMusic = false; //define que ha acabado de cambiar la música
    }

    public void procesarTextos(string[] listaDialogo)
    {
        personaje.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        personaje.moving = false;
        StartCoroutine(procesoListaString(KeyCode.Space, listaDialogo));
    }
    public void procesarTexto(string s)
    {
        personaje.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        personaje.moving = false;
        StartCoroutine(procesoString(KeyCode.Space, s));
    }
    private IEnumerator procesoListaString(KeyCode key, string[] listaDialogo)
    {
        personaje.interacting = true;
        personaje.canTalk = false;
        yield return new WaitForSeconds(0.1f);//tarda un poco para que te de tiempo a ver el primer texto
        textBox.gameObject.SetActive(true);
        bool done = false;
        for (int i = 0; i < listaDialogo.Length; i++)
        {
            dialogo.text = "";
            string s = listaDialogo[i];
            for (int j = 0; j < s.Length; j++)
            {
                dialogo.text += s[j];
                yield return new WaitForSeconds(0.01f);
            }
            done = false;
            while (!done) // essentially a "while true", but with a bool to break out naturally
            {
                if (Input.GetKeyDown(key))
                    done = true; // breaks the loop
                yield return null; // wait until next frame, then continue execution from here (loop continues)
            }
            yield return new WaitForSeconds(0.2f);
        }
        textBox.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        personaje.canTalk = true;
        personaje.interacting = false;
        // now this function returns
    }
    private IEnumerator procesoString(KeyCode key, string s)
    {
        personaje.interacting = true;
        personaje.canTalk = false;
        yield return new WaitForSeconds(0.1f);//tarda un poco para que te de tiempo a ver el primer texto
        textBox.gameObject.SetActive(true);
        dialogo.text = "";
        for (int j = 0; j < s.Length; j++)
        {
            dialogo.text += s[j];
            yield return new WaitForSeconds(0.01f);
        }
        bool done = false;
        while (!done) // essentially a "while true", but with a bool to break out naturally
        {
            if (Input.GetKeyDown(key))
                done = true; // breaks the loop
            yield return null; // wait until next frame, then continue execution from here (loop continues)
        }
        yield return new WaitForSeconds(0.1f);
        personaje.canTalk = true;
        personaje.interacting = false;
        // now this function returns
    }

    public void changePosition()
    {
        float distancia = 0.65f;
        float posX = personaje.transform.position.x;
        float posY = personaje.transform.position.y + 0.25f;
        int largo = 8;
        int ancho = 10;
        switch (personaje.direccion)
        {
            case Direction.Up: this.transform.position = new Vector2(posX, posY + distancia * 2); this.transform.localScale = new Vector2(ancho, largo); break;
            case Direction.Down: this.transform.position = new Vector2(posX, posY - distancia); this.transform.localScale = new Vector2(ancho, largo); break;
            case Direction.Left: this.transform.position = new Vector2(posX - distancia * 1.5f, posY); this.transform.localScale = new Vector2(largo, ancho); break;
            case Direction.Right: this.transform.position = new Vector2(posX + distancia * 1.5f, posY); this.transform.localScale = new Vector2(largo, ancho); break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Interactivo>() != null)
        {
            canTalk = true;
            interactivo = collision.gameObject.GetComponent<Interactivo>();
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Interactivo>() != null)
        {
            canTalk = false;
            interactivo = null;
        }
    }

}