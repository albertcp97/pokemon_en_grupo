﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class Save 
{
    public  Entrenador entrenador=null;
    public float px;
    public float py;
    public int [] entder;
    public int [] evac;

    public SerialInteract[] estadoInteract;
    public List<SerialInteract> listaInteract;

    public Save(){

    }
}
