﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCLayerSorter : MonoBehaviour
{
    private float timer;
    private Renderer playerRenderer;

    void Awake()
    {
        playerRenderer = GameObject.Find("Player").GetComponent<Renderer>();
    }

    void LateUpdate()
    {
        timer -= Time.deltaTime;
        if (timer <= 0f)
        {
            foreach (Transform child in transform)
            {
                if (child.GetComponent<Renderer>() != null)
                {
                    if (child.transform.position.y < playerRenderer.transform.position.y)
                        child.GetComponent<Renderer>().sortingOrder = playerRenderer.sortingOrder + 1;
                    else if (child.transform.position.y > playerRenderer.transform.position.y)
                        child.GetComponent<Renderer>().sortingOrder = playerRenderer.sortingOrder - 1;
                }
            }
        }
    }
}
