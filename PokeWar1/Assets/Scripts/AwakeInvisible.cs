﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class AwakeInvisible : MonoBehaviour
{
    private void Awake()
    {
        //Hacer collisiones invisibles al comenazar
        //Tilemap collisionsTilemap = GameObject.Find("Collisions").gameObject.GetComponent<Tilemap>();
        Color c = new Color();
        c.a = 0.0f;
        if(gameObject.GetComponent<Tilemap>()!=null)
            gameObject.GetComponent<Tilemap>().color = c;
        if (gameObject.GetComponent<SpriteRenderer>() != null)
            gameObject.GetComponent<SpriteRenderer>().color = c;
    }
}
