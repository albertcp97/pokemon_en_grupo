﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokemonButton : MonoBehaviour

{
     public GameObject pokemonNow;
    // Start is called before the first frame update
    void Start()
    {
        pokemonNow.SetActive(false);
    }

    // Update is called once per frame
    public void openMenu()
    {
        if (pokemonNow.activeInHierarchy)
        {
            pokemonNow.SetActive(false);
        }
        else
        {
            pokemonNow.SetActive(true);
            
        }
    }
}
