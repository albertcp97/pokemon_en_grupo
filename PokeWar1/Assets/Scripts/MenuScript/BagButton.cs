﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagButton : MonoBehaviour
{
    public GameObject menuNow;

    private PlayerController player;

    private void Start()
    {
        menuNow.SetActive(false);
        player = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    public void openMenu()
    {
        if (menuNow.activeInHierarchy)
        {
            menuNow.SetActive(false);
            player.interacting = false;
        }
        else
        {
            menuNow.SetActive(true);
            player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            player.canTalk = false;
            player.moving = false;
            player.interacting = true;
        }
    }
}
