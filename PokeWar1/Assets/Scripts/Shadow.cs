﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if(this.gameObject.transform.parent != null)
            this.gameObject.gameObject.GetComponent<SpriteRenderer>().sprite = this.gameObject.transform.parent.gameObject.GetComponent<SpriteRenderer>().sprite;
    }
}
