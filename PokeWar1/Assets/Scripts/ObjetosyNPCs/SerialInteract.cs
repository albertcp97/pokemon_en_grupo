﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SerialInteract //clase utilizada en interactivo que se guardara al guardar partida
{
    public bool activo;
    public int nUsos;//solo si se consideran un numero de usos

    public SerialInteract()
    {
        activo = true;
        nUsos = 5;
    }
    public SerialInteract(bool activo, int nUsos)
    {
        this.activo = activo;
        this.nUsos = nUsos;
    }

    override
    public string ToString()
    {
        return "activo: " + activo + ", nUsos:" + nUsos;
    }
}
