﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Evento
{
    public enum TipoEvento
    {
        Dialogo, PlaySE, PlayME, ChangeFog, ShowImage, ShowEmoticon
    }
    //Dialogo: Le pasas un string y se mostrara en un textBox
    //PlaySE: Reproducira un Efecto de sonido que coincida con el nombre puesto en la string texto
    //PlayME: Reproducira en bucle la música que coincida con el nombre puesto en la string texto
    //NOTA: Para poder reproducir la música/efectos de sonido hay que añadirla en el script de ControlDialogo
    //que lo contiene el objeto ControlDialogo que es uno de los hijos
    //en las listas llamadas SE y ME
    //ChangeFog: Cambiara en el animador el fog (clima) que coincida con el nombre puesto en la string texto (null para vaciar)
    //ShowImage: Mostrar 1 imagen indicada por texto
    //ShowEmoticon: Mostrar sobre el personaje un emotico durante 2 segundos


    public TipoEvento tipo = TipoEvento.Dialogo;
    public string texto;

    
    public void ejecutarEvento()
    {
        PlayerController player = GameObject.Find("Player").GetComponent<PlayerController>();
        ControlDialogo cD = GameObject.Find("ControlDialogo").GetComponent<ControlDialogo>();
        switch (tipo)
        {
            case Evento.TipoEvento.Dialogo: cD.procesarTexto(texto); break;
            case Evento.TipoEvento.PlaySE: cD.playSE(texto); break;
            case Evento.TipoEvento.PlayME: cD.playME(texto); break;
            case Evento.TipoEvento.ChangeFog: GameObject.Find("Fog").GetComponent<FogScript>().changeFog(texto); break;
            case Evento.TipoEvento.ShowImage: cD.showImage(texto); break;
            case Evento.TipoEvento.ShowEmoticon: cD.showEmoticon(texto); break;
        }
    }
}
