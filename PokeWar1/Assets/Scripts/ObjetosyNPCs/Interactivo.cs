﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

//[RequireComponent(typeof(Image))]
//[System.Serializable]
public class Interactivo : MonoBehaviour
{
    public enum TipoInteraccion
    {
        Pulsar, Colisionar
    }
    public enum Usos
    {
        SiempreActivo, UnSoloUso, NumeroUsos
    }
    protected PlayerController player = null;
    protected ControlDialogo cD = null;

    public SerialInteract si = new SerialInteract();//clase que guardaremos
    public TipoInteraccion tipoInteraccion;
    public Usos usos;
    public Evento[] eventos;

    protected void Awake()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        cD = GameObject.Find("ControlDialogo").GetComponent<ControlDialogo>();
    }
    protected void Update()
    {
        if (!player.eventing && GameObject.Find("TextBox") != null)
        {
            if (GameObject.Find("TextBox") != null)
                GameObject.Find("TextBox").SetActive(false);
            if (GameObject.Find("ImageBox") != null)
                GameObject.Find("ImageBox").SetActive(false);
        }
    }

    public virtual void Interactuar() //virtual para que sus hijos puedan hacer override
    {
        player.eventing = true;
        StartCoroutine(procesarEventos());
    }
    protected IEnumerator procesarEventos()
    {
        for (int i = 0; i < eventos.Length; i++)
        {
            Evento e = eventos[i];
            e.ejecutarEvento();
            bool done = false;
            while (!done) // essentially a "while true", but with a bool to break out naturally
            {
                if (!player.interacting)
                    done = true; // breaks the loop
                yield return null; // wait until next frame, then continue execution from here (loop continues)
            }
            //yield return new WaitForSeconds(0.1f);
        }
        player.eventing = false;
    }


    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (si.activo && collision.gameObject.name.Equals("Player") && this.tipoInteraccion == TipoInteraccion.Colisionar)
        {
            Interactuar();
            si.activo = false;
            if (this.usos == Usos.NumeroUsos && si.nUsos > 0)
                si.nUsos--;
        }
    }
    protected void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("Player") && this.tipoInteraccion == TipoInteraccion.Colisionar)
        {
            if (this.usos == Usos.SiempreActivo)
                this.si.activo = true;
            if (this.usos == Usos.NumeroUsos && si.nUsos > 0)
                this.si.activo = true;
        }
    }
    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (si.activo && collision.gameObject.name.Equals("Player") && this.tipoInteraccion == TipoInteraccion.Colisionar)
        {
            Interactuar();
            this.si.activo = false;
            if (this.usos == Usos.NumeroUsos && this.si.nUsos > 0)
                this.si.nUsos--;
        }
    }
    protected void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player") && this.tipoInteraccion == TipoInteraccion.Colisionar)
        {
            if (this.usos == Usos.SiempreActivo)
                this.si.activo = true;
            if (this.usos == Usos.NumeroUsos && this.si.nUsos > 0)
                this.si.activo = true;
        }
    }
}