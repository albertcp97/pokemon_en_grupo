﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class NPC : Interactivo
{
    private string npcId;
    public Sprite[] npcSprites;
    public Direction direccion;
    

    private void Start()
    {
        cambiarDireccion(direccion);
        //npcId = this.gameObject.name.Substring(0, 5);// sera de npc00 a npc99
    }

    public void mirarJugador()
    {
        switch (player.direccion)
        {
            case Direction.Up: cambiarDireccion(Direction.Down); break;
            case Direction.Down: cambiarDireccion(Direction.Up); break;
            case Direction.Left: cambiarDireccion(Direction.Right); break;
            case Direction.Right: cambiarDireccion(Direction.Left); break;
        }
    }
    public void cambiarDireccion(Direction nuevaDireccion)
    {
        SpriteRenderer npcSpriteRenderer = this.GetComponent<SpriteRenderer>();
        //Sprite[] npcSprites = Resources.LoadAll<Sprite>(npcId);
        switch (nuevaDireccion)
        {
            case Direction.Up: npcSpriteRenderer.sprite = npcSprites[3]; break;
            case Direction.Down: npcSpriteRenderer.sprite = npcSprites[0]; break;
            case Direction.Left: npcSpriteRenderer.sprite = npcSprites[1]; break;
            case Direction.Right: npcSpriteRenderer.sprite = npcSprites[2]; break;
        }
        this.direccion = nuevaDireccion;
    }
}
