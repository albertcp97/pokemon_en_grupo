﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class imageAnimation : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Image>().sprite = this.GetComponent<SpriteRenderer>().sprite;
    }
}
