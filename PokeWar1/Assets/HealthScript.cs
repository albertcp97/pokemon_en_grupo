﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    private PJController pj = null;
    // Start is called before the first frame update
    void Start()
    {
        pj = this.transform.parent.GetComponentInParent<PJController>();
        //Debug.Log(pj.vidaMax +" "+ pj.name);

    }

    // Update is called once per frame
    void Update()
    {
        decimal divideno = decimal.Divide(pj.vidaAct, pj.vidaMax);
        this.transform.localScale = new Vector2(1 * (float)divideno, this.transform.localScale.y);
        this.GetComponent<SpriteRenderer>().sortingOrder = this.transform.parent.GetComponent<SpriteRenderer>().sortingOrder+1;
    }
}
