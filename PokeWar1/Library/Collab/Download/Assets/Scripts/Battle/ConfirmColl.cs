﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ConfirmColl : MonoBehaviour
{
    private BattleSeleter bs = null;
    private PJController pj = null;

    public bool collides;
    public bool cuadraVerde;
    public bool sigueChocando;
    internal bool cuadraGroc;

    // Start is called before the first frame update
    private void Start()
    {
        pj = this.transform.GetComponentInParent<PJController>();
        bs = this.transform.GetComponentInParent<BattleSeleter>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        sigueChocando = true;
        if ((pj != null || bs != null) && collision.gameObject.GetComponent<Tilemap>()!=null)
            collides = true;
        else if (pj != null && collision.gameObject.name.Equals("Cuadra"))
            cuadraVerde = true;
        if (collision.gameObject.name.Equals("Cuadra"))
            cuadraVerde = true;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        sigueChocando = true;
        if ((pj != null || bs != null) && collision.gameObject.GetComponent<Tilemap>() != null)
            collides = true;
        if (pj != null && collision.gameObject.name.Equals("Cuadra"))
            cuadraVerde = true;
        if (collision.gameObject.name.Equals("Cuadra"))
            cuadraVerde = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        sigueChocando = false;
        if ((pj != null || bs != null) && collision.gameObject.GetComponent<Tilemap>() != null)
            collides = false;
        if (pj != null && collision.gameObject.name.Equals("Cuadra"))
            cuadraVerde = false;
        if (collision.gameObject.name.Equals("Cuadra"))
            cuadraVerde = false;
    }
}
