﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ActionManager : MonoBehaviour
{
    public GameObject cuadra;
    public GameObject cuadro;
    private EventSystem eventSys;
    private GameObject prota;

    private GameObject detectCuadra = null;
    float centroX;
    float centroY;
    public GameObject btMoverCancelar;
    public GameObject btMover;
    public GameObject btAtacar;
    public GameObject btPasarTurno;
    public GameObject btAtras;
    public GameObject btAttack1;
    public GameObject btAttack2;
    public GameObject btAttack3;
    public GameObject btAttack4;
    private void Start()
    {
        eventSys = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        prota = GameObject.FindGameObjectWithTag("Prota");

        detectCuadra = GameObject.Find("DetectCuadro");
    }
    public void color1()
    {
        Mover(1);

    }
    public void color2()
    {
        Mover(2);
    }
    public void Mover(int color)
    {
        PJController pjController = GameObject.Find("Selecter").GetComponent<PlayerSeleter>().pjController;
        float x = pjController.gameObject.transform.position.x;
        float y = pjController.gameObject.transform.position.y;
        int m = pjController.mov;
        centroX = x;
        centroY = y;
        llenarCasilla(x, y, m, color);//funcion recursiva
        
        btMover.SetActive(false);
        btAtacar.SetActive(false);
        btPasarTurno.SetActive(false);
        btAtras.SetActive(false);
        btAttack1.SetActive(false);
        btAttack2.SetActive(false);
        btAttack3.SetActive(false);
        btAttack4.SetActive(false);
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().btMoverCancelar.SetActive(true);

        pjController.canMove = true;

        //GameObject.Find("Selecter").GetComponent<BattleSeleter>().interactuando = true;

    }
    public void cancelarMover()
    {
        foreach (Transform child in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
        {
            Destroy(child.gameObject);
        }
        PJController pjController = GameObject.Find("Selecter").GetComponent<PlayerSeleter>().pjController;
        pjController.transform.position = new Vector2(centroX, centroY);
        pjController.canMove = false;
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().pjController = null;

        FuckGoBack();

    }

    private void llenarCasilla(float x, float y, int m, int color)
    {
        GameObject newCuadra;
        if (color == 1)
        {
            newCuadra = Instantiate(cuadra);
        }
        else
        {
            newCuadra = Instantiate(cuadro);
        }
        newCuadra.name = "Cuadra";
        newCuadra.transform.position = new Vector2(x, y);
        foreach (Transform child in GameObject.Find("padreCuadros").transform) //navego entre los objetos hijos
        {
            if (child.transform.position == newCuadra.transform.position)
                Destroy(child.gameObject);
        }
        newCuadra.transform.parent = GameObject.Find("padreCuadros").transform;
        if (m > 0)
        {
            centroX -= 1;
            llenarCasilla(x - 1, y, m - 1,color);//izquierda
            centroX += 2;
            llenarCasilla(x + 1, y, m - 1,color);//derecha
            centroX -= 1;

            centroY -= 1;
            llenarCasilla(x, y - 1, m - 1,color);//abajo
            centroY += 2;
            llenarCasilla(x, y + 1, m - 1,color);//arriba
            centroY -= 1;
        }
    }

    public void Atack()
    {
        GameObject.Find("ButtonMover").SetActive(false);
        GameObject.Find("ButtonAtacar").SetActive(false);
        GameObject.Find("ButtonPasarTurno").SetActive(false);
        GameObject.Find("ButtonAtras").SetActive(false);


        GameObject.Find("Selecter").GetComponent<BattleSeleter>().interactuando = false;
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().ataque = true;
        

    }
    public void Turn()
    {
        Debug.Log("Napo");
        eventSys.SetSelectedGameObject(null);
    }
    public void FuckGoBack()
    {
        GameObject.Find("Selecter").GetComponent<BattleSeleter>().interactuando = false;
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().btMoverCancelar.SetActive(false);
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().btMover.SetActive(false);
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().btAtacar.SetActive(false);
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().btPasarTurno.SetActive(false);
        GameObject.Find("Selecter").GetComponent<PlayerSeleter>().btAtras.SetActive(false);

        eventSys.SetSelectedGameObject(null);
    }


}
