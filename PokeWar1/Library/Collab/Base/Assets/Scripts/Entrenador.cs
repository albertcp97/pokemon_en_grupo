﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class Entrenador
{
    public string nombre;
    public string nombreGrafico;
    public int lvl;
    public int mov;
    public int hpmax;
    public int def;
    public int expdada;
    //protected int id;
    public List<Pokemon> listaPokemon = new List<Pokemon>();

    private List<Botiquin> inventario = new List<Botiquin>();

    public Entrenador(string nombre, int lvl, int mov, int hpmax, int def, int expdada, List<Pokemon> listaPokemon){
        //this.id=id;
        this.nombre = nombre;
        this.lvl = lvl;
        this.mov = mov;
        this.hpmax = hpmax;
        this.def = def;
        this.expdada = expdada;
        this.listaPokemon=listaPokemon;
    }

    public void setInventario(List<Botiquin> newinventario)
    {
        inventario = newinventario;
    }
    public List<Botiquin> getInventario()
    {
        return inventario;
    }

}

    


