﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour
{
    public GameObject menuNow;
    public AudioClip[] audioList;
    public GameObject pokemenu;
    private Animator menuAnimator;
    private Animator pokemonAnimator;
    private AudioSource menuAudio;
    private PlayerController player;
    private EventSystem eventSystem;
    public UnityEngine.UI.Text hora;
    public UnityEngine.UI.Text hora2;
    public UnityEngine.UI.Text hora3;
    public UnityEngine.UI.Text pokemon1;
    public UnityEngine.UI.Text inv1;
    public UnityEngine.UI.Image icon1;
    public GameObject inventario;
    public Pokemon pokemon;
    public String horas;
    public DateTime date;
    private bool coso = false;
    private bool coso2 = false;

    private void Start()
    {
        //menuNow.SetActive(false);
        menuAnimator = menuNow.GetComponent<Animator>();
        menuAudio = this.gameObject.GetComponent<AudioSource>();
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        esconderP();
        esconderI();
        
    }
    private void Update(){
       acHora();
    }
    public void openPoke(){
        if (coso){
            esconderP();
            
        }
        else {
           mostrarP();
          
        }
    }
     public void openInv(){
        if (coso2){
            esconderI();
            
        }
        else {
           mostrarI();
          
        }
    }
    public void esconderI(){
        inventario.SetActive(false);
        coso2=false;
    }
    public void mostrarI(){
        inventario.SetActive(true);
        coso2=true;
        Entrenador e = GameObject.Find("Player").GetComponent<PlayerController>().entrenador;
        List<Botiquin> bot = new List<Botiquin>();
        bot=e.inventario;
        String inv;
        inv=bot[0].ToString();
        inv1.text=inv;
    }
    public void esconderP(){
        pokemenu.SetActive(false);
        coso=false;
   
    }
    public void mostrarP(){
       pokemenu.SetActive(true);
        coso=true;
        Entrenador e = GameObject.Find("Player").GetComponent<PlayerController>().entrenador;
        pokemon=e.listaPokemon[0];
        pokemon1.text=pokemon.ToString();
        icon1.sprite=Resources.Load<Sprite>("Iconos/Absolicon");

    }
    public void openMenu()
    {
        if (menuAnimator.GetBool("showMenu"))
        {
            playSE("menuClose");
            menuAnimator.SetBool("showMenu", false);

            player.interacting = false;
           
        }
        else if(!player.interacting)
        {
            playSE("menuOpen");
            menuAnimator.SetBool("showMenu", true);

            player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            player.canTalk = false;
            player.moving = false;
            player.interacting = true;
        }
        eventSystem.SetSelectedGameObject(null);
    }
    
    public void acHora(){
         date = DateTime.Now;
        if (date.Minute<10){
            horas = ""+date.Hour+" 0"+date.Minute;
        }
        else {
            horas = ""+date.Hour+" "+date.Minute;
        }
        
        hora.text=horas;
        hora2.text=horas;
        hora3.text=horas;
    }
    public void playSE(string s)
    {
        //Debug.Log(s);
        foreach (AudioClip clip in audioList)
        {
            if (clip.name.Equals(s))
                menuAudio.PlayOneShot(clip);
        }
    }
    public void saveGame()
    {
        playSE("menuSave");
        GameObject.Find("Player").GetComponent<PlayerController>().saveGame();
        openMenu();//close
    }
    public void loadGame()
    {
        menuAudio.volume = 0.5f;
        playSE("menuLoad");
        menuAudio.volume = 1.0f;
        GameObject.Find("Player").GetComponent<PlayerController>().loadGame();
        openMenu();//close
    }

    public void listaPokemon(){

    }

}
