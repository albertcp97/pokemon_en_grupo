﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class PlayerController : MonoBehaviour
{
    public int vel = 2;

    public Direction direccion;

    private Rigidbody2D playerBody;
    private Animator playerAnimator;

    private bool moving_forward;
    private Vector2 forwardPosition;
    private int moving_forwardFrames = 0;
    private int npasos = 0;
    public bool moving;
    private bool running;
    public bool eventing;
    public bool interacting;
    public bool battling;
    public bool canTalk;
    public Entrenador entrenador = null;
    public Entrenador traienrEnemigo = null;
    private static GameObject player = null;
    int [] entder = null;
    int[] evac = null;

    private void Awake()
    {
        Application.targetFrameRate = 30;//fps del juego (move_Forward)
        //Player
        if (player == null)
        {
            player = this.gameObject;
           // Ataque cuchillada = new Ataque("Cuchillada","Normal",100,1,10,false,false,1);
            List<Movimiento> movs = new List<Movimiento>();
            movs.Add(new Movimiento("Mordisco", 50, 2));
            movs.Add(new Movimiento("Mordisco", 50, 2));
            movs.Add(new Movimiento("Mordisco", 50, 2));
            List<Botiquin> bot = new List<Botiquin>();
            Botiquin pocion = new Botiquin("Poción","Pociones","Este objeto curativo recupera 10 puntos de salud",10,5);
            bot.Add(pocion);
            //movs.Add(cuchillada);
            List<Pokemon> poks = new List<Pokemon>();
            Pokemon absol = new Pokemon("Absol",359, 6, 100,30,10,500,movs);//nombre, pokedex, alcanceMover, vida, ataque, defensa, expdada, movimientos
            poks.Add(absol);
            entrenador = new Entrenador("AAA", 10, 5, 200, 20, 500,poks);//nombre, nivel, alcanceMover, vida, defensa, expdada, listaPokemon
            entrenador.setInventario(bot);
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
            Destroy(this);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        playerBody = player.GetComponent<Rigidbody2D>();
        playerBody.gravityScale = 0;
        playerBody.constraints = RigidbodyConstraints2D.FreezeRotation;
        playerAnimator = player.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //cheat
        if (Input.GetKey("9"))
        {
            if(player.GetComponent<Collider2D>().isTrigger)
                player.GetComponent<Collider2D>().isTrigger = false;
            else
                player.GetComponent<Collider2D>().isTrigger = true;
        }
        if (SceneManager.GetActiveScene().buildIndex == 0 && battling)
        {
            battling = false;
        }
        else if (Input.GetKeyDown("m") && !battling && SceneManager.GetActiveScene().buildIndex == 0)
        {
            battling = true;
            moving = false;
            //SceneManager.LoadScene(1);

        }
        if (!battling && !moving_forward && !interacting && !eventing)
        {
            moveLibre();
        }
        else if (moving_forward)
            moveForwardAdvanced(0.75f);//0.32 una casilla
        holdAnimation();
        //moveForward();
         if (Input.GetKey("o"))
        {
            this.saveGame();
        }
        if (Input.GetKey("p"))
        {
            this.loadGame();
        }
    }

    private bool moveLibre()
    {
        float velActualX = 0;
        float velActualY = 0;
        bool up = Input.GetKey("w");
        bool left = Input.GetKey("a");
        bool down = Input.GetKey("s");
        bool right = Input.GetKey("d");
        running = Input.GetKey(KeyCode.LeftShift);
        if (down)//ABAJO
        {
            velActualY -= vel;
            if (((!(left || right) && velActualY != 0) || ((left && right) && velActualX == 0)) || shouldRun("PlayerDown"))
                cambiarDireccion(Direction.Down);
            if (!currentAnimationName().Equals("rPlayerDown") && !currentAnimationName().Equals("PlayerDown")) velActualY /= 2;
        }
        if (up)//ARRIBA
        {
            velActualY += vel;
            if ((!(left || right) && velActualY != 0) || ((left && right) && velActualX == 0) || shouldRun("PlayerUp"))
                cambiarDireccion(Direction.Up);
            if (!currentAnimationName().Equals("rPlayerUp") && !currentAnimationName().Equals("PlayerUp")) velActualY /= 2;
        }
        if (up && down) velActualY = 0; //tendria que hacerlo automatico pero no lo hace asi que añado este trozo de codigo
        if (left)//IZQUIERDA
        {
            velActualX -= vel;
            if (velActualX != 0 && velActualY == 0 || shouldRun("PlayerLeft"))
                cambiarDireccion(Direction.Left);
            if (!currentAnimationName().Equals("rPlayerLeft") && !currentAnimationName().Equals("PlayerLeft")) velActualX /= 2;
        }
        if (right)//DERECHA
        {
            velActualX += vel;
            if (velActualX != 0 && velActualY == 0 || shouldRun("PlayerRight"))
                cambiarDireccion(Direction.Right);
            if (!currentAnimationName().Equals("rPlayerRight") && !currentAnimationName().Equals("PlayerRight")) velActualX /= 2;
        }
        if (left && right) velActualX = 0; //tendria que hacerlo automatico pero no lo hace asi que añado este trozo de codigo
        if (running)//CORRER
        {
            velActualX *= 1.5f;
            velActualY *= 1.5f;
        }
        playerBody.velocity = new Vector2(velActualX, velActualY); //aplicar velocidad

        if (velActualX == 0 && velActualY == 0) moving = false;
        else moving = true;

        return moving;
    }

    //Devuelve true si el personaje se mueve en una direccion corriendo y el sprite que se ve está andando y viceversa, false si no es así
    private bool shouldRun(string s)
    {
        if((running && currentAnimationName().Equals(s)) || (!running && currentAnimationName().Equals("r"+s)))
        {
            return true;
        }
        return false;
    }

    private void holdAnimation()
    {
        if (!moving)
        {
            string s = currentAnimationName();
            if (s[0] == 'r') s = s.Substring(1);
            playerAnimator.Play(s, 0, 0);
        }
        else
        {
            cambiarDireccion(this.direccion);//Actualiza siempre que se mueva la animacion
        }
    }

    public string currentAnimationName()//metodo para saber que animacion se esta reproduciendo
    {
        var currAnimName = "";
        foreach (AnimationClip clip in playerAnimator.runtimeAnimatorController.animationClips)
        {
            if (playerAnimator.GetCurrentAnimatorStateInfo(0).IsName(clip.name))
                currAnimName = clip.name.ToString();
        }
        return currAnimName;
    }

    public bool cambiarDireccion(Direction nuevaDireccion)
    {
        string s = "Player" + nuevaDireccion.ToString();
        if (running) s = "r" + s;//saber si esta corriendo
        if (!currentAnimationName().Equals(s) || !moving)//esta tonteria de !moving hace que si pulsas muy seguido una tecla de WASD no parezca que te estas deslizando por el suelo
        {
            string r = "";
            if (running) r = "r";//saber si esta corriendo para añadir al nombre de animacion
            switch (nuevaDireccion)
            {
                case Direction.Up: playerAnimator.Play(r + "PlayerUp", 0, 0.25f); break;
                case Direction.Down: playerAnimator.Play(r + "PlayerDown", 0, 0.25f); break;
                case Direction.Left: playerAnimator.Play(r + "PlayerLeft", 0, 0.25f); break;
                case Direction.Right: playerAnimator.Play(r + "PlayerRight", 0, 0.25f); break;
            }
            this.direccion = nuevaDireccion;
            return false;
        }
        return true;
    }

    public void cambiarPosicion(Vector2 posicion, Direction direccion)//Funciona mejor la corutina de Teletransportador.cs que llama a esta funcion
    {
        transform.position = posicion;
        running = false;
        cambiarDireccion(direccion);
    }

    public void cambiarLayer(int nuevoLayer)
    {
        player.GetComponent<SpriteRenderer>().sortingOrder = nuevoLayer;
        foreach (Transform child in transform)
        {
            child.GetComponent<SpriteRenderer>().sortingOrder = nuevoLayer;
        }
    }

    private void moveForward()
    {
        moving_forwardFrames++;
        float velActualX = 0;
        float velActualY = 0;
        switch (this.direccion)
        {
            case Direction.Up: velActualY += vel; break;
            case Direction.Down: velActualY -= vel; break;
            case Direction.Left: velActualX -= vel; break;
            case Direction.Right: velActualX += vel; break;
        }
        playerBody.velocity = new Vector2(velActualX, velActualY); //aplicar velocidad
        if (moving_forwardFrames >= 100)
        {
            moving_forward = false;
            moving_forwardFrames = 0;
        }
    }
    private void moveForwardAdvanced(float distancia)//hay una mejor explicacion de lo que hace esto dentro de battleSelecter
    {
        Debug.Log("hola");
        Vector3 targetPosition = new Vector3(forwardPosition.x, forwardPosition.y); //inicializo objetivo
        switch (this.direccion)
        {
            case Direction.Up: targetPosition = new Vector3(forwardPosition.x, forwardPosition.y + distancia); break;
            case Direction.Down: targetPosition = new Vector3(forwardPosition.x, forwardPosition.y - distancia); break;
            case Direction.Left: targetPosition = new Vector3(forwardPosition.x - distancia, forwardPosition.y); break;
            case Direction.Right: targetPosition = new Vector3(forwardPosition.x + distancia, forwardPosition.y); break;
        }
        //Direccion en la que se mueve (1,0)=derecha (-1,0)=izquierda (0,1)=arriba (0,-1)=abajo //0 1 aparecen por el normalized
        Vector3 moveDir = (targetPosition - transform.position).normalized;
        float moveAmount = vel * Time.deltaTime;
        moveAmount = Mathf.Clamp(moveAmount, 0f, Vector3.Distance(transform.position, targetPosition));
        transform.position += moveDir * moveAmount;
        if (Vector3.Distance(transform.position, targetPosition) == 0.0f)
        {
            npasos--;
            if (npasos <= 0)
            {
                moving_forward = false;
                this.moving = false;
            }
            else //en caso de tener un numero de pasos pendiente de hacer seguira avanzando
            {
                this.forwardPosition = this.transform.position;
            }
        }
    }
    public void activeForwardMovement()
    {
        npasos = 1;
        this.forwardPosition = this.transform.position;
        this.moving_forward = true;
        this.moving = true;
    }
    public void activeForwardMovement(int pasos)
    {
        if (pasos < 0) npasos = 0;
        else npasos = pasos;
        this.forwardPosition = this.transform.position;
        this.moving_forward = true;
        this.moving = true;
    }

    private void interact()//repracated
    {
        canTalk = false;
        moving = false;
        interacting = true;
        //interacting = false;
    }
    public void startBattle(Entrenador e, int scene)
    {
        if (!battling)
        {
            traienrEnemigo = e;
            battling = true;
            moving = false;
            SceneManager.LoadScene(scene);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!battling && !moving_forward && !interacting && !eventing)
            GameObject.Find("ControlDialogo").GetComponent<ControlDialogo>().playSE("playerBump");
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<PuntoTeletransporte>() != null)
        {

        }
    }
    private Save createSave()
    {
        Save save= new Save();
        save.entrenador=this.entrenador;
        save.px = this.transform.position.x;
        save.py = this.transform.position.y;
        save.entder = this.entder;
        save.evac = this.evac;

        //Extra eventosHistoria
        GameObject actualEventHistoria = GameObject.Find("EventHistoria");
        save.listaInteract = new List<SerialInteract>();
        foreach (Transform child in actualEventHistoria.transform) //navego entre los objetos hijos
        {
            Interactivo eventoInteract = child.GetComponent<Interactivo>();
            save.listaInteract.Add(new SerialInteract(eventoInteract.si.activo, eventoInteract.si.nUsos));
        }

        return save;

    }
      public void saveGame()
    {
        Save save = this.createSave();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);
        file.Close();
        //Completado
        Debug.Log("Game Saved");

    }
    public void loadGame()
    {
        // 1
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            
            // 2
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            file.Close();

            // 3
            this.transform.position = new Vector2(save.px, save.py);
            //Extra eventosHistoria
            GameObject actualEventHistoria = GameObject.Find("EventHistoria");
            if(actualEventHistoria.transform.childCount == save.listaInteract.Count)
            {
                int i = 0;
                foreach (Transform child in actualEventHistoria.transform) //navego entre los objetos hijos
                {
                    Interactivo eventoInteract = child.GetComponent<Interactivo>();
                    eventoInteract.si = new SerialInteract(save.listaInteract[i].activo, save.listaInteract[i].nUsos);
                    i++;
                }
            }
            else
                Debug.Log("El numero de eventos guardado es diferente a los del juego");
            //Completado
            Debug.Log("Game Loaded");
            GameObject.Find("ControlDialogo").GetComponent<ControlDialogo>().resetAudio();

        }
        else
        {
            Debug.Log("No game saved!");
        }
    }

}
