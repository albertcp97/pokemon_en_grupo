﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;



public class PlayerSeleter : MonoBehaviour
{
    public bool dale = false;
    public PJController pjController = null;

    public GameObject btMoverCancelar;
    public GameObject btMover;
    public GameObject btAtacar;
    public GameObject btPasarTurno;
    public GameObject btAtras;
    public GameObject btAttack1;
    public GameObject btAttack2;
    public GameObject btAttack3;
    public GameObject btAttack4;
    public bool ataque = false;
    private EventSystem even;
    private 
    // Start is called before the first frame update
    void Start()
    {
        btMoverCancelar = GameObject.Find("ButtonMoverCancelar");
        btMover = GameObject.Find("ButtonMover");
        btAtacar = GameObject.Find("ButtonAtacar");
        btPasarTurno = GameObject.Find("ButtonPasarTurno");
        btAtras = GameObject.Find("ButtonAtras");
        btAttack1 = GameObject.Find("ButtonAtacar1");
        btAttack2 = GameObject.Find("ButtonAtacar2");
        btAttack3 = GameObject.Find("ButtonAtacar3");
        btAttack4 = GameObject.Find("ButtonAtacar4");
        btMoverCancelar.SetActive(false);
        btMover.SetActive(false);
        btAtacar.SetActive(false);
        btPasarTurno.SetActive(false);
        btAtras.SetActive(false);
        btAttack1.SetActive(false);
        btAttack2.SetActive(false);
        btAttack3.SetActive(false);
        btAttack4.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (dale && !this.gameObject.GetComponent<BattleSeleter>().interactuando) {
           
            if (Input.GetKey(KeyCode.Space) && !this.gameObject.GetComponent<BattleSeleter>().moving)

            {   
                this.gameObject.GetComponent<BattleSeleter>().interactuando = true;
                btMover.SetActive(true);
                btAtacar.SetActive(true);
                btPasarTurno.SetActive(true);
                btAtras.SetActive(true);
                if (ataque)
                {
                    btMover.SetActive(false);
                    btAtacar.SetActive(false);
                    btPasarTurno.SetActive(false);
                    btAtras.SetActive(false);
                    btAttack1.SetActive(true);
                    btAttack2.SetActive(true);
                    btAttack3.SetActive(true);
                    btAttack4.SetActive(true);

                }


            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Prota" || collision.gameObject.GetComponent<PJController>()!=null)
        {
            dale = true;
            pjController = collision.gameObject.GetComponent<PJController>();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.gameObject.tag == "Prota" || collision.gameObject.GetComponent<PJController>() != null) && !this.gameObject.GetComponent<BattleSeleter>().interactuando)
        {
            dale = false;
            pjController = null;
        }
    }

}
